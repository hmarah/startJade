package fundamentals.createPlatform;
import jade.core.Agent;


/**
 * Do nothing.
 * This empty agent is instantiated when the createPlatform exemple is triggered
 * @author hc
 *
 */
public class AgentEmpty extends Agent{

	private static final long serialVersionUID = 8844980834348115888L;

}
